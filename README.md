# Library Card Maker

Part of my job involves making training videos for public libraries and their staff. Whenever I need to use a picture of a library card in the video, I always try to grab an image of the actual library card for that system. But sometimes, I just can't. It could be that there's no time, or that no one knows where the image files are, or there's a distinct lack of an image, or whatever else. When I need a library card, and can't get one, I use my favourite stand-in: A mock library card I made for Miskatonic Public Library, where ***Your mind is a terrible thing to waste.***

<img align="left" style="padding: 5px 10px 5px 5px;" src="https://i.imgur.com/JHD0AQ1.jpg" alt="Miskatonic Public Library" width="200">

Riffing on the stories of H.P. Lovecraft, Miskatonic Public Library is a spectacular place filled with beautiful books and the histories of a strange religious sect. It's a beautiful library. Just don't read any of the books out loud.

I brought this up in a silly Facebook conversation and a few friends said "I WANT MY OWN MISKATONIC LIBRARY CARD!" So I thought to myself, I could really use another beer... oh and maybe I could write something in PHP.

This toy uses ImageMagick to build the library card, so you'll want to make sure that's installed on your webserver along with the standard PHP extension for ImageMagick.

![Miskatonic Public Library Card](https://i.imgur.com/WSS3Z6J.jpg)

#### Credits

[Start Bootstrap](https://startbootstrap.com/templates/simple-sidebar/) - Simple Sidebar. Released under an MIT License.
