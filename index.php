<?php
//Don't cache the site or weird things happen.
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Library Card Maker - Miskatonic Public Library</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/simple-sidebar.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">

<form action="createcard.php" target="iframe" method="post" enctype="multipart/form-data">
<div>

<h2>Miskatonic Public Library</h2>
<h4>Make your own card</h4>

<label>
	<span>Your name (ex. Jane Cochran):</span> <input type="text" name="name"><br>
</label>

<label>
	<span>Your library card number (10 - 13 digits work best):</span> <input type="text" maxlength="13" name="cardnumber"><br>
</label>

<script>
function setURL(url){
	document.getElementById('iframe').src = url;
}
</script>
<button type="submit">Make my card</button>
<br>
<p style="font-size: 11px;"><a href="index.php">Start over.</a></p>
</div>
</form>

        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<div id="bname"><h4>Your card will appear here.</h4></div>
                        <iframe name="iframe" width="900" height="900" frameborder="no" border="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</div>


</body>

</html>
